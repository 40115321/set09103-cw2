from flask import render_template, flash, redirect, url_for, request, \
    session

from app import app
from models import *
from app import db
import re

from back import *


@app.route('/')
def index():

    threads = Thread.query.order_by(Thread.upvotes >= 0).limit(5)
    users = []
    posts = []
    subs = []

    for t in threads:
        u = User.query.filter_by(id=t.user_id).first()
        users.append(u)

        p = Post.query.filter_by(thread_id=t.id).count()
        posts.append(p)

        s = Sub.query.filter_by(id=t.sub_id).first()
        subs.append(s)

    al = zip(threads, users, posts, subs)


    return render_template('base.html', page='index', subs=get_sub("all"), user=get_current_user(), threads=al)


"""USER MANAGEMENT SECTION"""


@app.route('/signup', methods=['GET', 'POST'])
def signup():
    if request.method == 'POST':
        if register_user_account(request.form['usernameField'], request.form['passwordField'], request.form['emailField']):
            return redirect(url_for('index'))
        else:
            return get_message(4)
    return redirect(url_for('index'))


@app.route('/login', methods=["GET", "POST"])
def login():
    if request.method == 'POST':
        if request.form['login']:
            result = log_user_in(
                request.form['usernameField'], request.form['passwordField'])

            if result == 1:
                flash(get_message(9))
                return back.redirect()
            else:
                flash(result)

    return redirect(url_for('index'))


@app.route('/logout')
def logout():
    if 'USERNAME' in session:
        session.pop('USERNAME', None)
        flash(get_message(8))
        return back.redirect()
    return redirect.back()


@app.route('/user')
def user():
    return redirect(url_for('index'))


@app.route('/user/<int:user_id>/settings')
@back.anchor
def user_settings(user_id):

    if 'USERNAME' in session:
        user = get_current_user()
        if user.id == user_id:

            pms = PM.query.filter_by(recipient=user.username) \
                .order_by(PM.date_sent.desc()).all()

            return render_template('base.html', page='user_settings', subs=get_sub("all"), user=get_current_user(), pms=pms)
    return redirect(url_for('index'))


@app.route('/user/<int:user_id>/update', methods=["GET", "POST"])
def update_details(user_id):
    if 'USERNAME' in session:
        user = get_current_user()
        if user.id == user_id:

            if request.method == 'POST':
                if request.form["passwordOld"] != "" and request.form["passwordNew"] != "":
                    if user.check_password(request.form['passwordOld']):
                        user.set_password_hash(request.form['passwordNew'])
                        add_item_to_database(user)
                        flash(get_message(12))
                        return redirect(url_for('logout'))

                elif request.form['usernameNew'] != "":
                    if check_if_username_exists(request.form['usernameNew']) is False:
                        user.username = request.form['usernameNew']
                        add_item_to_database(user)
                        flash(get_message(13))
                        return redirect(url_for('logout'))

                elif request.form['emailNew'] != "":
                    if check_if_email_exists(request.form['emailNew']) is False:
                        user.email = request.form['emailNew']
                        add_item_to_database(user)
                        flash(get_message(14))
                        return redirect(url_for('logout'))
                flash(get_message(1))
    return redirect(url_for('index'))


"""SUBS RELATED"""


@app.route('/sub/create', methods=['GET', 'POST'])
def create_sub():
    if request.method == 'POST':
        sub = sub_create(request.form['sub_title'])
        return redirect(url_for('sub_title', sub_title=sub.title, page=1, subs=get_sub("all"), user=get_current_user()))
    return render_template('base.html', page='create_sub', subs=get_sub("all"), user=get_current_user())


@app.route('/sub/<sub_title>/page/<int:page>')
@app.route('/sub/<sub_title>/page/<int:page>/<sort>')
@back.anchor
def sub_title(sub_title, page, sort=None):
    if page is None:
        page = 1

    threads = get_paginated_threads(sub_title, page, sort)
    pagination = get_threads(sub_title, page, sort)

    return render_template('base.html', sub_title=sub_title, threads=threads, pagination=pagination, page='sub_threads', subs=get_sub("all"), user=get_current_user())


"""THREADS RELATED"""


@app.route('/sub/<sub_title>/<int:thread_id>', methods=['GET', 'POST'])
@back.anchor
def sub_thread(sub_title, thread_id):

    thread = get_thread("id", thread_id)
    thread_creator = User.query.filter_by(id=thread.user_id).first()

    al = Post.query.filter_by(thread_id=thread_id).filter_by(parent_id=0).all()

    return render_template('base.html', page='thread', thread=thread,
        thread_creator=thread_creator, subs=get_sub("all"), sub_title=sub_title, al=al, user=get_current_user())


@app.route('/sub/<sub_title>/create', methods=['GET', 'POST'])
def create_thread(sub_title):
    is_link = False

    if 'USERNAME' in session:
        if request.method == 'POST':
                if request.form['isLink'] == "False":
                    is_link = False
                else:
                    is_link = True


                if request.form['titleField'] != "" and request.form['contentField'] != "":
                    thread = thread_create(request.form['titleField'], request.form['contentField'],
                        is_link, sub_title)

                    return redirect(url_for('sub_thread', sub_title=sub_title, thread_id=thread.id, subs=get_sub("all"), user=get_current_user()))
                flash(get_message(10))
                return redirect(url_for('sub_title', sub_title=sub_title, page=1))
    flash(get_message(7))
    return redirect(url_for('index'))


@app.route('/sub/<sub_title>/<int:thread_id>/create', methods=['GET', 'POST'])
def create_post(sub_title, thread_id):

    if 'USERNAME' in session:
        if request.method == 'POST':
            val = request.form['hiddenreply']
            if val is None:
                val = 0

            user = User.query.filter_by(username=session['USERNAME']).first()

            if request.form['contentField'] != "":
                post_create(user, thread_id, request.form['contentField'], val)
                return redirect(url_for('sub_thread', sub_title=sub_title, thread_id=thread_id, user=get_current_user()))
            flash(get_message(6))
            return redirect(url_for('sub_thread', sub_title=sub_title, thread_id=thread_id))
    flash(get_message(7))
    return redirect(url_for('index'))


@app.route('/sub/<sub_title>/<int:thread_id>/upvote', methods=['GET', 'POST'])
def upvote_thread(sub_title, thread_id):
    if 'USERNAME' in session:
        thread = get_thread("id", thread_id)
        thread.upvotes += 1
        add_item_to_database(thread)
        return redirect(url_for('sub_thread', sub_title=sub_title, thread_id=thread_id, user=get_current_user()))
    flash(get_message(7))
    return redirect(url_for('sub_title', sub_title=sub_title, page=1))


@app.route('/sub/<sub_title>/<int:thread_id>/downvote', methods=['GET', 'POST'])
def downvote_thread(sub_title, thread_id):
    if 'USERNAME' in session:
        thread = get_thread("id", thread_id)
        thread.downvotes += 1
        add_item_to_database(thread)
        return redirect(url_for('sub_thread', sub_title=sub_title, thread_id=thread_id, user=get_current_user()))

    flash(get_message(7))
    return redirect(url_for('sub_title', sub_title=sub_title, page=1))

@app.route('/sub/<sub_title>/<int:thread_id>/upvote/<post_id>', methods=['GET', 'POST'])
def upvote_post(sub_title, thread_id, post_id):
    if 'USERNAME' in session:
        post = get_post("id", post_id)
        post.upvotes += 1
        add_item_to_database(post)
        return redirect(url_for('sub_thread', sub_title=sub_title, thread_id=thread_id, user=get_current_user()))

    flash(get_message(7))
    return redirect(url_for('sub_thread', sub_title=sub_title, thread_id=thread_id, user=get_current_user()))


@app.route('/sub/<sub_title>/<int:thread_id>/downvote/<post_id>', methods=['GET', 'POST'])
def downvote_post(sub_title, thread_id, post_id):
    if 'USERNAME' in session:
        post = get_post("id", post_id)
        post.downvotes += 1
        add_item_to_database(post)
        return redirect(url_for('sub_thread', sub_title=sub_title, thread_id=thread_id, user=get_current_user()))

    flash(get_message(7))
    return redirect(url_for('sub_thread', sub_title=sub_title, thread_id=thread_id, user=get_current_user()))


@app.route('/user/send_message', methods=['GET', 'POST'])
@back.anchor
def send_pm():

    if request.method == 'POST':
        if 'USERNAME' in session:
            user = User.query.filter_by(username=request.form['hiddenRec']).first()
            if user:
                pm = PM(get_current_user().username, user.username, request.form['subjectField'], request.form['messageField'])
                add_item_to_database(pm)
                flash(get_message(15))

        else:
            flash(get_message(7))
    return redirect(url_for('index'))


@app.errorhandler(404)
def page_not_found(error):
    app.logger.error("404 - %s not found", (request.path))
    return '404 - Page not found'


""" User Account Functions """


def register_user_account(username, password, email):

    """ Register the user account

        :username
        :password
        :email

        Validates all of the user account credentials during signup
    """
    if validate_username(username) and validate_email(email):
        if not check_if_username_exists(username) and not check_if_email_exists(email):
            create_user_account(username, password, email)
            return True
    return False


def create_user_account(username, password, email):

    """ Create a user account using given information

        :username - Must be unique
        :password - Can be any text
        :email - is checked to make sure it is somewhat valid

        Logs the user in after successful account creation
    """

    user = User(username, password, email)
    session['USERNAME'] = user.username
    add_item_to_database(user)


def log_user_in(username, password):

    """ Logs the user into the system

        :username
        :password

        Checks to make sure values are not empty
        and if the username is a valid account
        Compares hashed password to hash value of given password
    """

    if username != "" and password != "":
        user = User.query.filter_by(username=username).first()
        if user:
            if user.check_password(password):
                session['USERNAME'] = user.username
                return 1
            return get_message(3)
        return get_message(2)
    return get_message(3)


def get_current_user():

    """ Returns current user that is logged in

        Returns 'None' if no user is logged in
    """

    if 'USERNAME' in session:
        return User.query.filter_by(username=session['USERNAME']).first()
    return None


def validate_email(email):

    """ Validates email against regex pattern

        :email - must follow pattern: abc@email.com

        Returns True/False whether successful
    """

    pattern = '[a-zA-Z0-9._-]+@[a-z-A-Z-0-9]+[.]+[a-z-A-Z]+[.]*[a-z]*'
    if re.match(pattern, email) is not None:
        return True
    return False


def validate_username(username):

    """ Validates username against regex pattern

        :username - must be atleast 3 characters/numbers long

        Returns True/False whether successful
    """

    pattern = '[a-z-A-Z-0-9]{3,}'
    if re.match(pattern, username) is not None:
        return True
    return False


def check_if_username_exists(username):

    """ Checks if given username already exists in database

        :username - username given from user 

    """

    user = User.query.filter_by(username=username).first()
    if user:
        return True
    return False


def check_if_email_exists(email):

    """ Checks if given email already exists in database

        :email - email given from user

    """

    user = User.query.filter_by(email=email).first()
    if user:
        return True
    return False

""" User Account Functions End """

""" Sub Functions Start """


def sub_create(sub_title):

    """ Creates a sub using the given data

        :sub_title - The title for the sub, is the primary key

        Replaces all spaces in the sub title to stop %20 in url
    """

    sub = Sub(sub_title.replace(' ', '_'))
    add_item_to_database(sub)
    return sub


def get_sub(criteria, value=None):

    """ Returns sub object using given criteria to filter by
        :title
        :id
        :all
    """

    if criteria == "title":
        return Sub.query.filter_by(title=value).first()
    elif criteria == "id":
        return Sub.query.filter_by(id=value).first()
    elif criteria == "all":
        return Sub.query.all()

""" Sub Functions End """


""" Thread Functions Start """


def thread_create(thread_title, thread_content, is_link, sub_title):

    """ Creates a thread using the given data

        :thread_title - title of thread
        :thread_content - thread content - either stores text for discussions, link for link
        :is_link - boolean stores whether the thread is discussion or link
        :sub_title - used to link thread to given sub
    """

    """Get logged in user"""
    currentUser = get_current_user()

    sub = get_sub("title", sub_title)

    thread = Thread(
        thread_title, thread_content, is_link, currentUser.id, sub.id)
    add_item_to_database(thread)

    return thread


def get_threads(sub_title, page, sort):

    """ Returns paginated threads to fix issue with .has_next in template"""

    sub = get_sub("title", sub_title)

    max_per = int(app.config['MAX_THREADS_PER_PAGE'])
    #max_per = 5
    threads = sort_threads(sub.id, sort).paginate(page, max_per)

    return threads


def get_paginated_threads(sub_title, page, sort):

    """ Returns zipped list containing the paginated threads,
        the users who posted the threads and the amount of posts
        in the thread

        app.config['MAX_THREADS_PER_PAGE']: is configurable in config.cfg
    """

    sub = get_sub("title", sub_title)

    max_per = int(app.config['MAX_THREADS_PER_PAGE'])
    #max_per = 5
    threads = sort_threads(sub.id, sort).paginate(page, max_per)

    users = []
    posts = []

    for t in threads.items:
        u = User.query.filter_by(id=t.user_id).first()
        users.append(u)

        p = Post.query.filter_by(thread_id=t.id).count()
        posts.append(p)

    return zip(threads.items, users, posts)


def sort_threads(sub_id, sort):

    """ Returns all threads in a sub sorted by certain criteria

        :links - returns threads that are links
        :discussions - returns threads that are discussions
        :oldest - returns oldest threads desc
        :newest - returns newest threads desc
        :upvoted - returns threads ordered by most upvotes
        :downvoted - returns threads ordered by most downvotes
        :posts - returns threads ordered by most posts
    """

    if sort == "links":
        return Thread.query.filter_by(sub_id=sub_id).filter_by(link=True)
    elif sort == "discussions":
        return Thread.query.filter_by(sub_id=sub_id).filter_by(link=False)
    elif sort == "oldest":
        return Thread.query.filter_by(sub_id=sub_id).order_by(Thread.date_created)
    elif sort == "newest":
        return Thread.query.filter_by(sub_id=sub_id).order_by(Thread.date_created.desc())
    elif sort == "upvoted":
        return Thread.query.filter_by(sub_id=sub_id).order_by(Thread.upvotes.desc())
    elif sort == "downvoted":
        return Thread.query.filter_by(sub_id=sub_id).order_by(Thread.downvotes.desc())
    elif sort == "posts":
        pass
    else:
        return Thread.query.filter_by(sub_id=sub_id)


def get_thread(criteria, value=None):

    """ Returns thread object using given criteria to filter by
        :title
        :id
        :sub id
        :sub title
        :all
    """

    if criteria == "title":
        return Thread.query.filter_by(title=value).first()
    elif criteria == "id":
        return Thread.query.filter_by(id=value).first()
    elif criteria == "sub_id":
        return Thread.query.filter_by(sub_id=value).all()
    elif criteria == "sub_title":
        return Thread.query.filter_by(sub_title=value).all()
    elif criteria == "all":
        return Thread.query.all()

""" Thread Functions End """

""" Post Functions Start """


def post_create(user, thread_id, post_content, parent_id):
    post = Post(post_content, user.id, thread_id, parent_id)
    add_item_to_database(post)


def get_post(criteria, value=None):

    """ Return all posts within a thread

        :thread_id - id of the thread to filter by

        Returns None should the thread_id not exist
    """
    if criteria == "id":
        return Post.query.filter_by(id=value).first()
    elif criteria == "thread_id":
        return Post.query.filter_by(thread_id=value).first()
    elif criteria == "all":
        return Post.query.filter_by(thread_id=value).all()


""" Post Functions End """


""" Miscelaneous Fucntions """


def get_message(error):

    """ Return message as a string

        :1 - Something went wrong
        :2 - Username does not exist
        :3 - Please make sure your username and password are correct
        :4 - Signup process has failed. Please try again
        :5 - Post title must not be blank
        :6 - Post content must not be blank
        :7 - You must be logged in
        :8 - You have been logged out
        :9 - Welcome back <user>
        :10 - Thread title must not be blank
        :11 - Thread content must not be blank
        :12 - Password has been changed
        :13 - Username has been changed
        :14 - Email has been changed
        :15 - Private Message Sent
    """

    if error == 1:
        return "Error - Something went wrong."
    elif error == 2:
        return "Error - Username does not exist."
    elif error == 3:
        return "Error - Please make sure your username and password are correct."
    elif error == 4:
        return "Error - Signup process has failed. Please try again."
    elif error == 5:
        return "Error - Post title must not be blank."
    elif error == 6:
        return "Error - Post content must not be blank."
    elif error == 7:
        return "Error - You must be logged in."
    elif error == 8:
        return "You have been logged out."
    elif error == 9:
        return "Welcome back"
    elif error == 10:
        return "Error - Thread title must not be blank."
    elif error == 11:
        return "Error - Thread content must not be blank."
    elif error == 12:
        return "Password has been changed."
    elif error == 13:
        return "Username has been changed."
    elif error == 14:
        return "Email has been changed."
    elif error == 15:
        return "Private Message Sent."


def add_item_to_database(item):

    """ Commit given item to database

        :item - any data that can be stored as model
    """

    db.session.add(item)
    db.session.commit()


""" Miscelaneous Functions End """
