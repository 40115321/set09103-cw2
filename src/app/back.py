from flask import session, redirect, request, url_for
import functools

class back(object):

    """ Code taken from http://flask.pocoo.org/snippets/120/ by  Danya Alexeyevsky
    
    To be used in views.

    Use 'anchor' decorator to mark a view as a possible point of return.

    'url()' is the last saved url.

    Use `redirect` to return to the last return point visited.

    """

    cookie = "back"
    default_view = "index"

    @staticmethod
    def anchor(func, cookie=cookie):
        @functools.wraps(func)
        def result(*args, **kwargs):
            session[cookie] = request.url
            return func(*args, **kwargs)
        return result

    @staticmethod
    def url(default=default_view, cookie=cookie):
        return session.get(cookie, url_for(default))

    @staticmethod
    def redirect(default=default_view, cookie=cookie):
        return redirect(back.url(default, cookie))


back = back()
