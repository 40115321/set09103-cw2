from werkzeug.security import generate_password_hash, check_password_hash
from app import db
from datetime import datetime


class User(db.Model):

    """ User Model
        
        :id - primary key, autoincrements for each new user account created
        :username - stores username for account, must be unique
        :password_hash - stores hashed value of password instead of actual password
        :email - stores primary email of account, must be unique 

        :subscribed_subs - stores subs that a user is subscribed to 

    """

    __tablename__ = 'user'
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), unique=True)
    password_hash = db.Column(db.String(128))
    email = db.Column(db.String(120), unique=True)

    def __init__(self, username, password, email):
        self.username = username
        self.set_password_hash(password)
        self.email = email

    """set salted password hash"""
    def set_password_hash(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)


class Sub(db.Model):

    """ Sub Model
        
        :id - primary key, autoincrements for each new sub created
        :title - stores title for sub, must be unique, is used frequently when querying

        :threads - relationship showing that a sub can contain many threads

    """

    __tablename__ = 'sub'
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(256), unique=True)

    threads = db.relationship('Thread', backref='sub', lazy='dynamic')

    def __init__(self, title):
        self.title = title


class Thread(db.Model):

    """ Thread Model
        
        :id - primary key, autoincrements for each new thread created
        :title - stores title for thread
        :content - either text or link
        :date_created - holds the date/time that thread was created
        :link - boolean stores whether thread is discussion or link

        :posts - threads can have many posts within
        :sub_id - a thread is always stored within a sub
        :user_id - a thread is always created by a user

        :threads - relationship showing that a sub can contain many threads

    """

    __tablename__ = 'thread'
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(256))
    content = db.Column(db.String(512))

    sub_id = db.Column(db.Integer, db.ForeignKey('sub.id'))
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))

    date_created = db.Column(db.DateTime)

    link = db.Column(db.Boolean, default=False)

    upvotes = db.Column(db.Integer)
    downvotes = db.Column(db.Integer)

    posts = db.relationship('Post', backref='thread', lazy='dynamic')

    def __init__(self, title, content, link, user_id, sub_id, date_created=None, upvotes=None, downvotes=None):
        self.title = title
        self.content = content
        self.link = link
        self.user_id = user_id
        self.sub_id = sub_id

        if date_created is None:
            date_created = datetime.utcnow()
        self.date_created = date_created

        if upvotes is None:
            upvotes = 0
        self.upvotes = upvotes

        if downvotes is None:
            downvotes = 0
        self.downvotes = downvotes

    def get_vote_rating(self):
        return self.upvotes - self.downvotes


class Post(db.Model):

    """ Post Model
        
        :id - primary key, autoincrements for each new post created
        :content - stores post content, either text or link
        
        :parent_id - stores the id of parent post
        :children - relationship showing posts can be replies of other posts

        :date_created - holds the date/time that post was submitted

        :sub_id - a user is always associated with a post
        :thread_id - a post is always stored within a thread 

    """

    __tablename__ = 'post'
    id = db.Column(db.Integer, primary_key=True)
    content = db.Column(db.String(512))

    parent_id = db.Column(db.Integer, db.ForeignKey('post.id'))
    children = db.relationship('Post')

    date_created = db.Column(db.DateTime)

    upvotes = db.Column(db.Integer)
    downvotes = db.Column(db.Integer)

    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    thread_id = db.Column(db.Integer, db.ForeignKey('thread.id'))

    def __init__(self, content, user_id, thread_id, parent_id=None, date_created=None, upvotes=None, downvotes=None):
        self.content = content
        self.user_id = user_id
        self.thread_id = thread_id
        self.parent_id = parent_id

        if date_created is None:
            date_created = datetime.utcnow()
        self.date_created = date_created

        if upvotes is None:
            upvotes = 0
        self.upvotes = upvotes

        if downvotes is None:
            downvotes = 0
        self.downvotes = downvotes

    def get_vote_rating(self):
        return self.upvotes - self.downvotes


class PM(db.Model):

    """ Private Message Model

        :id - Primary key for message

        :sender - Username of sender of message
        :recipient - Username of reciever of message

        :title - Subject line
        :message - Message content
        
        :date_sent - UTC date of time sent

    """
    __tablename__ = 'pm'
    id = db.Column(db.Integer, primary_key=True)

    sender = db.Column(db.String(64), db.ForeignKey('user.username'))
    recipient = db.Column(db.String(64), db.ForeignKey('user.username'))

    title = db.Column(db.String(512), nullable=False)
    message = db.Column(db.String(512))

    date_sent = db.Column(db.DateTime)


    def __init__(self, sender, recipient, title, message, date_sent=None):
        self.sender = sender
        self.recipient = recipient
        self.title = title
        self.message = message

        if date_sent is None:
            date_sent = datetime.utcnow()
        self.date_sent = date_sent
