from flask import Flask
from logging.handlers import RotatingFileHandler
from flask_sqlalchemy import SQLAlchemy
import os, ConfigParser, logging
from datetime import datetime

basedir = os.path.abspath(os.path.dirname(__file__))
SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'app.db')

app = Flask(__name__)
app.config.from_object(__name__)
app.secret_key = os.urandom(24)
db = SQLAlchemy(app)

WTF_CSRF_ENABLED = True

from app import views, models

#Create db
#db.create_all()


#Read the config file
def init(app):
    config = ConfigParser.ConfigParser()
    try:
        config_location = os.path.join(basedir, 'config.cfg')
        config.read(config_location)

        #Application
        app.config['DEBUG'] = config.get('config', 'debug')
        app.config['IP_ADDRESS'] = config.get('config',  'ip_address')

        app.config['WTF_CSRF_ENABLED'] = \
            config.get('config', 'WTF_CSRF_ENABLED')

        app.config['MAX_THREADS_PER_PAGE'] = \
            config.get('config', 'MAX_THREADS_PER_PAGE')

        #Logging 
        app.config['LOG_FILE'] = config.get('logging', 'name')
        app.config['LOG_LOCATION'] = config.get('logging', 'location')
        app.config['LOG_LEVEL'] = config.get('logging', 'level')

    except:
        print 'Could not read config from ', config_location


#setup the logger
def logger(app):
    log_pathname = os.path.join(
        basedir, app.config['LOG_LOCATION'] + '\\' + app.config['LOG_FILE'])

    file_handler = RotatingFileHandler(
        log_pathname, maxBytes=1024*1024*10, backupCount=1024)

    file_handler.setLevel(app.config['LOG_LEVEL'])
    formatter = logging.Formatter('%(levelname)s | %(asctime)s | %(module)s \
        | %(funcName)s | %(message)s')

    file_handler.setFormatter(formatter)
    app.logger.setLevel(app.config['LOG_LEVEL'])
    app.logger.addHandler(file_handler)


def get_user(user_id):
    return models.User.query.filter_by(id=user_id).first()

app.jinja_env.globals.update(get_user=get_user)


@app.template_filter()
def timesince(dt, default="just now"):
    """
    Returns string representing "time since" e.g.
    3 days ago, 5 hours ago etc.

    Code taken from http://flask.pocoo.org/snippets/33/ by Dan Jacob
    """

    now = datetime.utcnow()

    diff = now - dt

    periods = (
        (diff.days / 365, "year", "years"),
        (diff.days / 30, "month", "months"),
        (diff.days / 7, "week", "weeks"),
        (diff.days, "day", "days"),
        (diff.seconds / 3600, "hour", "hours"),
        (diff.seconds / 60, "minute", "minutes"),
        (diff.seconds, "second", "seconds"),
    )

    for period, singular, plural in periods:

        if period:
            return "%d %s ago" % (period, singular if period == 1 else plural)

    return default

app.jinja_env.globals.update(timesince=timesince)
