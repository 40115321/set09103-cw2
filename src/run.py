from app import app, init, logger

init(app)
logger(app)

app.run(host=app.config['IP_ADDRESS'], debug=app.config['DEBUG'])
